package server

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestNewConfig(t *testing.T) {
	testCases := []struct {
		name    string
		isError bool
	}{
		{
			name:    "test 1",
			isError: false,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {

			assert.Equal(t, tc.isError, tc.isError)
		})
	}
}
